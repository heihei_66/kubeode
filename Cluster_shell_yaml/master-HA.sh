#!/bin/bash
#master节点2上面执行
IP=$(hostname -I |xargs -n 1   | grep  $(ip route |head  -n 1 | awk    '{print  $3}'  |  awk  -F  '.'  '{print  $1"."$2"."$3}')) ||IP=$(hostname -I |xargs -n 1   | grep  $(ip route |head  -n 1 | awk    '{print  $3}'  |  awk  -F  '.'  '{print  $1"."$2"."}'))

. /root/K8s/ip_2.txt
master_ip=${master_hosts_}
# master_ip=10.10.8.170
KUBE_ETC=/etc/kubernetes
KUBE_API_CONF=/etc/kubernetes/apiserver.conf
scp  $master_ip:/usr/local/bin/{kube-apiserver,kube-scheduler,kube-controller-manager,kubectl,helm}  /usr/local/bin/

# scp !(kubelet-client-*) -r $master_ip:/etc/kubernetes/  /etc/
rsync -avz -e ssh --exclude='kubelet-client-*' --exclude='kube-proxy.conf' --exclude='kube-proxy.kubeconfig'  --exclude='kubelet*'   root@$master_ip:/etc/kubernetes/  /etc/kubernetes/


# scp  -r $master_ip:/etc/kubernetes/pod-01/*  /etc/kubernetes/pod/
# scp  $master_ip:$KUBE_ETC/token.csv  $KUBE_ETC/
# scp  $master_ip:$KUBE_API_CONF  $KUBE_API_CONF
scp  $master_ip:/usr/lib/systemd/system/kube-apiserver.service  /usr/lib/systemd/system/kube-apiserver.service
. /root/K8s/ip_2.txt
sed  "s/=${master_hosts_}/=${IP}/g"  $KUBE_API_CONF  -i

systemctl daemon-reload
systemctl enable kube-apiserver.service --now
systemctl status kube-apiserver.service

#kube-scheduler
KUBE_ETC=/etc/kubernetes
KUBE_SCHEDULER_CONF=$KUBE_ETC/kube-scheduler.conf

scp  $master_ip:/usr/lib/systemd/system/kube-scheduler.service  /usr/lib/systemd/system/kube-scheduler.service
systemctl daemon-reload
systemctl enable kube-scheduler.service --now
sleep 10
systemctl status kube-scheduler.service
# kube-controller服务
KUBE_CONTROLLER_CONF=/etc/kubernetes/kube-controller-manager.conf
scp  $master_ip:/usr/lib/systemd/system/kube-controller-manager.service  /usr/lib/systemd/system/kube-controller-manager.service

systemctl daemon-reload
systemctl enable kube-controller-manager.service --now
sleep 10
systemctl status kube-controller-manager.service



kubectl get cs
grep completion /root/.bash_profile  || echo   'source <(kubectl completion bash)'  >> /root/.bash_profile
grep helm /etc/profile || echo  'source <(helm completion bash)'  >>/etc/profile



# ipvsadm -ln
#node节点修改vip地址
sed  "s/${master_ip}/${IP}/g"  -i  /etc/kubernetes/*config
sed  "s/${master_ip}/${IP}s/g"  -i  /etc/kubernetes/ssl/bootstrap.kubeconfig
# sed  "s/=${master_ip}/=10.103.97.123/g"  -i /etc/kubernetes/apiserver.conf
# ansible node -m shell -a "sed  \"s/${master_ip}/10.103.97.123/g\"  -i  /etc/kubernetes/*config"
# ansible node -m shell -a "sed  \"s/${master_ip}/10.103.97.123/g\"  -i  /etc/kubernetes/ssl/bootstrap.kubeconfig"
systemctl restart kube-proxy
systemctl restart kubelet
#
#
sleep 10
/usr/local/bin/kubectl label node ${IP}  node-role.kubernetes.io/Master_Ha=true
/usr/local/bin/kubectl label node ${master_ip}  node-role.kubernetes.io/Master_Ha=true